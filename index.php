<?php

require 'utils/functions.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $result = performForm();
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        showFeedbackFormResponse($result);
    } else {
        showFeedbackForm($result);
    }
} else {
    showFeedbackForm();
}

function showFeedbackForm($result = null)
{
    showTemplate('feedback-form', $result);
}

function showFeedbackFormResponse($result)
{
    showJson($result);
}

function performForm()
{
    $result = array();
    try {
        $name = $_POST['name'];
        $tel = $_POST['tel'];
        $email = $_POST['email'];
        $text = $_POST['text'];


        if (!strlen($name)) throw new Exception('Поле ФИО не заполнено');
        if (!strlen($tel)) throw new Exception('Поле Телефон не заполнено');
        if (!strlen($email)) throw new Exception('Поле E-mail не заполнено');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new Exception('Поле E-mail заполнено некорректно');


        $textResult = <<<EOT
ФИО: $name
Телефон: $tel
E-mail: $email
Сообщение:
$text
EOT;
        $subject = 'Новое сообщение с defa-job-test';


        sendEmail(ADMIN_EMAIL, $subject, $textResult);
        addLog($textResult);


        $result['success'] = true;
        $result['message'] = 'Спасибо. Ваше сообщение успешно отправлено.';
    } catch (Exception $e) {
        $result['error'] = true;
        $result['message'] = $e->getMessage();
    }

    return $result;
}
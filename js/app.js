App = {};

App.isEmail = function (email) {
    return /^[^@]+@[^@.]+\.[^@]+$/.test(email);
};

App.validateForm = function ($form) {
    var isOk = true;

    $form.find('input,textarea').each(function () {
        var $this = $(this);
        if (!App.validateField($this)) {
            isOk = false;
        }
    });

    return isOk;
};

App.validateField = function ($field) {
    var result = App.getFieldErrors($field).length === 0;
    if (result) {
        $field.removeClass('error');
    } else {
        $field.addClass('error');
    }
    return result;
};

App.getFieldErrors = function ($field) {
    var errors = [];

    if ($field.prop('required') && !$field.val()) {
        errors.push('Поле обязательно для заполнения');
    }

    if ($field.prop('type') === 'email' && !App.isEmail($field.val())) {
        errors.push('В поле нужно ввести E-mail');
    }

    return errors;
};
$('.js-ajax-form').each(function () {
    var self = $(this);
    var $form = self.find('.js-form');

    self.on('submit', function (ev) {
        ev.preventDefault();

        if (!App.validateForm($form)) {
            return false;
        }

        $.ajax({
            url: $form.prop('action'),
            type: $form.prop('method') || 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (result) {
                self.find('.js-form').addClass('hidden');
                self.find('.js-success').removeClass('hidden')
                    .find('.js-message').text(result['message']);
            },
            error: function (xhr, resp, text) {
                self.find('.js-form').addClass('hidden');
                self.find('.js-error').removeClass('hidden')
                    .find('.js-message').text(xhr.responseText);
            }
        });

        return false;
    });

    self.on('keyup', 'input,textarea', function () {
        var $field = $(this);
        App.validateField($field);
    });

    self.on('click', '.js-error,.js-success', function () {
        self.find('.js-form').removeClass('hidden');
        $(this).addClass('hidden');
    });
});



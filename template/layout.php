<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>DEFA job test</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="wrapper">
    <div class="header">

    </div>

    <div class="container">
        <div class="content">
            <?= $contentHtml ?>

        </div>


        <div class="aside">

        </div>

    </div>

    <div class="footer">

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script>
    if (typeof jQuery == 'undefined') {
        document.write(unescape("%3Cscript src='./js/jquery-3.0.0.min.js' type='text/javascript'%3E%3C/script%3E"));
    }
</script>
<script src="./js/app.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
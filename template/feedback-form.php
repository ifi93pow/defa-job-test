<div class="feedback-form js-ajax-form">
    <h1>Форма обратной связи</h1>
    <form action="" method="post" class="js-form<? if (isset($result['message'])) { ?> hidden<? } ?>">

        <label>
            ФИО
            <input type="text" name="name" value="" required>
        </label>

        <label>
            Телефон
            <input type="tel" name="tel" value="" required>
        </label>

        <label>
            E-mail
            <input type="email" name="email" value="" required>
        </label>

        <label>
            Комментарий
            <textarea name="text"></textarea>
        </label>

        <input type="submit" value="Отправить">

    </form>
    <div class="success-block js-success<? if (!isset($result['success'])) { ?> hidden<? } ?>">
        <p class="js-message"><?= isset($result['success']) && isset($result['message']) ? $result['message'] : '' ?></p>
    </div>
    <div class="error-block js-error<? if (!isset($result['error'])) { ?> hidden<? } ?>">
        <p>Ошибка при отправлении сообщения.</p>
        <p class="js-message"><?= isset($result['error']) && isset($result['message']) ? $result['message'] : '' ?></p>
    </div>
</div>
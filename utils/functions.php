<?php
define('ADMIN_EMAIL', 'test@example.com');
define('FROM_EMAIL', 'noreply@example.com');
define('LOG_FILENAME', 'log/messages.log');
define('BASE_PATH', dirname(__DIR__));

function sendEmail($to, $subject, $message, $from = FROM_EMAIL)
{
    mail($to, $subject, $message, "From: $from\r\n");
}

function addLog($message)
{
    $message = date('c') . "\n" . $message . "\n\n---\n\n";
    @mkdir(dirname(LOG_FILENAME));
    file_put_contents(LOG_FILENAME, $message, FILE_APPEND);
}

function showTemplate($template, $result = array())
{
    if (!is_array($result)) {
        $result = array();
    }

    ob_start();
    $template = preg_replace('[^a-z-]', '', $template);
    require BASE_PATH . "/template/$template.php";
    $contentHtml = ob_get_clean();

    require BASE_PATH . '/template/layout.php';
}

function showJson($result)
{
    if (!empty($result['error'])) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
        echo $result['message'];
    } else {
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}